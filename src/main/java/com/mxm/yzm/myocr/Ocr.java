package com.mxm.yzm.myocr;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Ocr {

    public static void main(String[] args) {
        OcrApi.myocr.init();
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\main\\resources\\static\\yzm.jpg"));
            //可能溢出,简单起见就不考虑太多,如果太大就要另外想办法，比如一次传入固定长度byte[]
            byte[] bytes  = new byte[fin.available()];
            //将文件内容写入字节数组，提供测试的case
            fin.read(bytes);
            fin.close();
            String yzm = OcrApi.myocr.ocr(bytes,bytes.length);
            System.out.println("验证码："+yzm);
        } catch (IOException e) {
            System.out.println("报错");
            // e.printStackTrace();
        }
    }

}
