package com.mxm.yzm.myocr;

import com.sun.jna.Library;
import com.sun.jna.Native;

public interface OcrApi extends Library {
	//最新版loadLibrary方法过时，应用load方法
    OcrApi myocr = (OcrApi) Native.loadLibrary(System.getProperty("user.dir")+"\\src\\main\\resources\\MyOcr.dll",OcrApi.class);
    void init();//初始化dll
    String ocr(byte[] z_bin,int ok);//图片验证码识别
}
